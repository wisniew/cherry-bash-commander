# Cherry Bash Commander

Let's onfigure kernel in terminal!<br />
Focus to be open, simple and easy to use.
<br />

### Key features:
* Support Android & Sailfish OS
* Show informations about hardware, software
* Monitor temperatures, memory usage, CPU freq
* Change CPU governor, governor tunables, max/min freq
* Change KCAL settings
* Change I/O scheduler
* Change basic memory variables
* Change Audio settings
* Create and manage own init.d file with all settings
* Clear cache *(Pagecache, dentries, inodes)*
* Flash multiple .zip files *(automatically reboot to TWRP, install, reboot to OS)*
* Integration with youtube-dl *(to download video, audio)*
* Updates itself with updater system *(stable, dev channels)*
* Emergency variables *(force update without running whole cbc)*

### To Do
* Profiles
* Cherry Deamon
* CPU input boost edit tab
* GPU edit tab
* Never-ending improvements
* Never-ending fixes

### Known issues
* CBC-installer.zip can't copy to /bin folder on android 10 *(manual install is recommended)*
<br />

### Sailfish OS

#### Tested:
* Oneplus 5t

#### Requirements:
* Bash *(preinstalled in OS)*
* CURL *(preinstalled in OS)*
* Python *(preinstalled in OS)*
* FingerTerm *(preinstalled in OS, shown in developer mode)*

#### Full install instruction:
* Download cbc.sh or cbc-dev.sh
* Copy cbc*.sh to ~/home/
* Rename cbc*.sh to cbc
* Follow steps below

#### How to run:
To run CBC execute in FingerTerm:
* devel-su bash cbc
<br />

### Android

#### Tested:
* Oneplus 5 *(only custom ROMs)*
* Oneplus 5t *(only custom ROMs)*

#### Requirements:
* Busybox *(installs via CBC-installer)*
* CURL *(installs via CBC-installer)*
* Python *(installs via CBC-installer)*
* Termux
* Termux:API *(for notifications, Cherry Deamon)*

#### Full install instruction:
* Install Termux app *(from google play)*
* Install Termux:API app *(from google play)*
* Run Termux and close
* Reboot to recovery
* Flash latest CBC-installer.zip
* Reboot to system
* Follow steps below

#### How to run:
To run CBC execute in termux:
* cbc
